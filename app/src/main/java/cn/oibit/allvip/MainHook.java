package cn.oibit.allvip;

import cn.oibit.allvip.hooks.JFM_Hook;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class MainHook implements IXposedHookLoadPackage {
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        // 加菲猫 去广告
        String packageName = lpparam.packageName;
        if (packageName.equals("com.jfm202203")) {
            JFM_Hook.hook(lpparam);
        }
    }
}
