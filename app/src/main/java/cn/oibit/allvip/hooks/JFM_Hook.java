package cn.oibit.allvip.hooks;


import android.content.Context;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class JFM_Hook {

    static Context context;
    static ClassLoader classLoader;


    public static void hook(XC_LoadPackage.LoadPackageParam lpparam) {
        Class<?> PostCardClass = XposedHelpers.findClass("com.alibaba.android.arouter.facade.Postcard", lpparam.classLoader);
        Class<?> NavigationCallbackClass = XposedHelpers.findClass("com.alibaba.android.arouter.facade.callback.NavigationCallback", lpparam.classLoader);

        //  获取真正的classLoader
        XposedHelpers.findAndHookMethod("com.alibaba.android.arouter.launcher._ARouter", lpparam.classLoader, "navigation", Context.class, PostCardClass, int.class, NavigationCallbackClass, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                context = (Context) param.args[0];
                if (context != null) {
                    classLoader = context.getClassLoader();
                    if (classLoader != null) {
                        hookMethod();
                    }
                }

            }
        });
    }

    public static void hookMethod() {
        // hook 启动页面
        XposedHelpers.findAndHookMethod("com.video.test.javabean.SplashBean", classLoader, "getPicUrl", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                param.setResult(null);
            }
        });

        // hook 首页弹窗
        XposedHelpers.findAndHookMethod("com.video.test.module.homepage.HomepageActivity", classLoader, "K0", List.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                param.args[0] = null;
            }
        });

        // hook 轮播图
        Class<?> bannerBeanClass = XposedHelpers.findClass("com.video.test.javabean.BannerBean", classLoader);
        XposedHelpers.findAndHookMethod("com.video.test.module.videotype.BaseVideoTypeListFragment", classLoader, "n3", List.class, List.class, List.class, int.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                List URL = (List) param.args[0];
                List content = (List) param.args[1];
                List layout = (List) param.args[2];
                int i = 0;
                ArrayList<Integer> indexes = new ArrayList<Integer>();
                for (Object object : layout) {
                    Method getType = bannerBeanClass.getDeclaredMethod("getType");
                    String type = (String) getType.invoke(object);
                    if (type.equals("3")) {
                        indexes.add(i++);
                    }
                }
                indexes.sort((Integer int1, Integer int2) -> int2 - int1); // tips: 逆序删除
                for (Integer index : indexes) {
                    URL.remove(index.intValue());
                    content.remove(index.intValue());
                    layout.remove(index.intValue());
                }
                param.args[0] = URL;
                param.args[1] = content;
                param.args[2] = layout;
            }
        });

        // hook 公告广告
        XposedHelpers.findAndHookMethod("com.video.test.module.videorecommend.VideoRecommendFragment", classLoader, "K2", List.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                param.args[0] = null;
            }
        });

        // hook 主页notice广告
        XposedHelpers.findAndHookMethod("com.video.test.module.videorecommend.VideoRecommendPresenter", classLoader, "y", List.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                param.args[0] = null;
            }
        });

        // hook 中间广告
        XposedHelpers.findAndHookMethod("com.video.test.module.videotype.BaseVideoTypeListPresenter", classLoader, "j", int.class, Context.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                param.args[0] = 2;
            }
        });

        //hook 播放页广告，自定义类参数
        Class<?> adInfoClass = XposedHelpers.findClass("com.video.test.javabean.AdInfoBean", classLoader);
        XposedHelpers.findAndHookMethod("com.video.test.module.player.PlayerActivity", classLoader, "o1", adInfoClass, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                param.args[0] = null;
            }
        });

        // 个人中心广告
        XposedHelpers.findAndHookMethod("com.video.test.module.usercenter.UserCenterFragment", classLoader, "s0", adInfoClass, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
                param.args[0] = null;
            }
        });

        // hook 版本升级
        XposedHelpers.findAndHookMethod("com.video.test.javabean.VersionInfoBean$InfoBean", classLoader, "getVersions", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                param.setResult(0);
            }
        });
    }
}
